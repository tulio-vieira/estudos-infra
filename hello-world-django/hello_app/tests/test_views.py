import pytest
from django.test import Client

@pytest.mark.django_db
def test_hello_world_view():
    res = Client().get("/")
    assert res.content == b"Hello, world."
