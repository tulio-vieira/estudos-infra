## Tarefa Infra

Dockerizar servidor django e base de dados postgres conforme a [tarefa](https://gitlab.com/camposmoreira/grupo-infra/-/blob/quarto-encontro/Guias/8%20-%20Tarefinha.MD).


### Comandos relevantes:
- decodar strings:
```
echo '<encoded-string>' | base64 --decode
```
- encodar strings:
```
echo '<string-to-encode>' | base64
```
- Fazer login em registry de imagens docker:
```
docker login registry.example.com -u <username> -p <token>
```
- Criar secret para autenticar com gitlab registry;
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-hello-world-django --docker-password=p-WNTUz9RSBfxF_oY2xG
```


### Links interessantes:

- [Criar um kubeconfig para o Amazon EKS](https://docs.aws.amazon.com/pt_br/eks/latest/userguide/create-kubeconfig.html)
- [How to use GitLab as a container registry for Kubernetes](https://discourse.charmhub.io/t/how-to-use-gitlab-as-a-container-registry-for-kubernetes/2642)
- [Using Gitlab Registry with Kubernetes](https://lionelmace.github.io/iks-lab/gitlab-registry.html)
- [Running database migrations using jobs and init containers](https://andrewlock.net/deploying-asp-net-core-applications-to-kubernetes-part-8-running-database-migrations-using-jobs-and-init-containers/)
- [Guto Carvalho - Do commit no GitLab ao deploy K8s, Descomplicando a Pipeline](https://www.youtube.com/watch?v=hnc3GD_gWEA)
